/**
 * Created by Bryce York (byork@fusionlabs.com.au).
 */

/* global angular:true, dragula:true, $:true */
(function () {
    "use strict";

    angular.module('myServices', [])
        .service('interactionHandlers', function () {
            this.click = function (str, e) {
                var elem = $(e.target).closest('.panel');

                var clearActive = function () {
                    if (document.getElementById('auto-close').checked) {
                        $('.' + str + ' > .panel').removeClass('active')
                            .find('.controls .toggle-state').removeClass('mdi-navigation-unfold-less').addClass('mdi-navigation-unfold-more');
                    }
                };

                if ($(elem).hasClass('active')) {
                    $(elem).find('.controls .toggle-state').addClass('mdi-navigation-unfold-more').removeClass('mdi-navigation-unfold-less');
                    $(elem).removeClass('active');
                    clearActive();
                } else {
                    clearActive();
                    $(elem).addClass('active');
                    $(elem).find('.controls .toggle-state').removeClass('mdi-navigation-unfold-more').addClass('mdi-navigation-unfold-less');
                }
            };

            this.toggleAll = function (trigger) {
                if (trigger === 1) {
                    // expand
                    $('#auto-close').attr('checked', false);
                    $('.phase > .panel').addClass('active');
                    setTimeout(function () { $('.step > .panel').addClass('active'); }, 250);
                } else {
                    // collapse
                    $('.step > .panel').removeClass('active');
                    setTimeout(function () { $('.phase > .panel').removeClass('active'); }, 300);
                }
            };
        })
        .service('roadmapAPI', function ($http, $log) {
            // TODO: use id arg to construct the api endpoint to be used by $http.get()
            //noinspection JSUnusedLocalSymbols
            this.get = function (id) {
                return $http.get('js/fake-api-endpoint.json').then(function (response) {
                    return response;
                }, function (data, status) {
                    $log.error(status + " error occured while retrieving the roadmap JSON.");
                });
            };
        });

    angular.module('myComponents', ['myServices'])
        .directive('leafPanel', function () {
            return {
                restrict: 'E',
                transclude: true,
                template: "<article class=\"panel panel-default\"><div class=\"panel-body\" ng-transclude>PANEL LABEL HERE</div></article>",
                replace: true
            };
        })
        .directive('navbar', function () {
            return {
                restrict: 'E',
                template: "<nav class=\"navbar navbar-default navbar-fixed-top shadow-z-1\"><div class=\"container\"><section class=\"navbar-header text-center\"><span class=\"navbar-brand\">Innovation Portal: Roadmap Editor</span></section></div></nav>",
                replace: true
            };
        })
        .directive('parentPanel', function (interactionHandlers) {
            //noinspection JSUnusedLocalSymbols
            return {
                restrict: 'E',
                transclude: true,
                template: "<article class=\"panel panel-default\"><div class=\"panel-body\"><span ng-transclude>PANEL LABEL HERE</span><span class=\"controls\"><i class=\"toggle-state mdi-navigation-unfold-more\"></i><span></div></article>",
                replace: true,
                link: function (scope, element, attrs) {
                    scope.click = interactionHandlers.click;
                }
            };
        })
        .directive('removalDropzone', function () {
            return {
                restrict: 'E',
                template: "<aside class=\"dropzone dropzone-delete navbar-fixed-bottom\">Drop Here To Remove From Roadmap</aside>",
                replace: true
            };
        })
        .directive('undoNotice', function () {
            return {
                restrict: 'E',
                transclude: true,
                template: "<aside class=\"navbar navbar-default navbar-danger navbar-fixed-bottom\"><div class=\"container\"><p class=\"text-center lead mts mbxs pbxs\"><span ng-transclude>NOTICE</span>&nbsp;<a class=\"btn btn-link btn-lg mtn mbn\">Undo Delete</a></p></div></aside>",
                replace: true
            };
        });


    // --- Main Module --- //
    angular.module('roadmapBuilder', ['myComponents', 'myServices'])
        .run(function ($log, $timeout) {
            $timeout(function () {
                function myDrakeBuilder (wrapper, target) {
                    target = target || wrapper.slice(0, -1); // if target isn't set, assume it's just the singular version of wrapper

                    return dragula({
                        isContainer: function (el) {
                            return el.classList.contains(wrapper) || el.classList.contains('dropzone');
                        },
                        invalid: function (el) {
                            return $($(el).closest('.' + target).find('.panel:first')).hasClass('active');
                        }
                    });
                }

                $.material.init();

                myDrakeBuilder('phases'); // declares phaseDrake
                myDrakeBuilder('steps'); // declares stepDrake
                myDrakeBuilder('modules'); // declares moduleDrake
            });
        })
        .controller('interfaceController', function ($scope, $log, interactionHandlers, roadmapAPI) {
            $scope.toggleAll = interactionHandlers.toggleAll;

            roadmapAPI.get().then(function (data) {
                $scope.data = data.data;
                $log.info('Successfully pulled data from service');
            });
        });

}());
